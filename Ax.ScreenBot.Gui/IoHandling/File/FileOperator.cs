﻿using Ax.ScreenBot.Model.Actions;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;

namespace Ax.ScreenBot.Gui.IoHandling.File
{
    internal static class FileOperator
    {
        public static void Serialize(ObservableCollection<Action> obj, string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<Action>));
                serializer.Serialize(fs, obj);
            }
        }

        public static ObservableCollection<Action> Deserialize(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<Action>));
                return (ObservableCollection<Action>)serializer.Deserialize(fs);
            }
        }
    }
}