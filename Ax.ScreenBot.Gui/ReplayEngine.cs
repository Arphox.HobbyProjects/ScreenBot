﻿using Ax.ScreenBot.Gui.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Action = Ax.ScreenBot.Model.Actions.Action;

namespace Ax.ScreenBot.Gui
{
    internal sealed class ReplayEngine
    {
        private readonly Action<string> _progressNotificationTextUpdate;
        private CancellationTokenSource _cancellationTokenSource;
        private CancellationToken _cancellationToken;

        public ReplayEngine(Action<string> progressNotificationTextUpdate)
        {
            _progressNotificationTextUpdate = progressNotificationTextUpdate ?? throw new ArgumentNullException(nameof(progressNotificationTextUpdate));
            RefreshCancellationToken();
        }

        private void RefreshCancellationToken()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = _cancellationTokenSource.Token;
        }

        public async Task StartAsync(ObservableCollection<Action> actions, uint numberOfRuns)
        {
            RefreshCancellationToken();

            bool progressNotificationTextSet = false;
            try
            {   
                await DoPreReplayCountdownAsync();
                _progressNotificationTextUpdate("Replay started.");
                await DoReplayAsync(actions, numberOfRuns);
            }
            catch (TaskCanceledException)
            {
                progressNotificationTextSet = true;
                _progressNotificationTextUpdate("Replay canceled.");
            }
            finally
            {
                if (!progressNotificationTextSet)
                    _progressNotificationTextUpdate("Replay finished.");
            }
        }

        public void Stop() => _cancellationTokenSource.Cancel();

        private async Task DoPreReplayCountdownAsync()
        {
            for (int i = 0; i < Settings.Default.Replayer_WaitBeforeReplaySeconds; i++)
            {
                _progressNotificationTextUpdate(
                    $"Starting in {Settings.Default.Replayer_WaitBeforeReplaySeconds - i} s.");

                await Task.Delay(TimeSpan.FromSeconds(1), _cancellationToken);
                if (_cancellationToken.IsCancellationRequested)
                    break;
            }
        }
        
        private async Task DoReplayAsync(IReadOnlyCollection<Action> actions, uint numberOfRuns)
        {
            if (_cancellationToken.IsCancellationRequested) return;

            for (int i = 0; i < numberOfRuns; i++)
            {
                foreach (var action in actions)
                {
                    await action.PerformAsync(_cancellationToken);
                    if (_cancellationToken.IsCancellationRequested)
                        return;
                }
            }
        }
    }
}