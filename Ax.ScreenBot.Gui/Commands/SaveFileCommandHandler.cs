﻿using System;
using System.Collections.ObjectModel;
using Ax.ScreenBot.Gui.IoHandling.File;
using Action = Ax.ScreenBot.Model.Actions.Action;

namespace Ax.ScreenBot.Gui.Commands
{
    internal sealed class SaveFileCommandHandler
    {
        private readonly ObservableCollection<Action> _storedActions;

        public SaveFileCommandHandler(ObservableCollection<Action> storedActions)
        {
            _storedActions = storedActions ?? throw new ArgumentNullException(nameof(storedActions));
        }

        public void Execute()
        {
            var win = new Microsoft.Win32.SaveFileDialog();
            win.ValidateNames = true;
            win.InitialDirectory = Environment.CurrentDirectory;

            if (win.ShowDialog() == true)
            {
                string fileName = System.IO.Path.GetFullPath(win.FileName);
                string fileExt = System.IO.Path.GetExtension(win.FileName);

                if (fileExt == string.Empty)
                {
                    fileName += ".xml";
                }

                FileOperator.Serialize(_storedActions, fileName);
            }
        }
    }
}