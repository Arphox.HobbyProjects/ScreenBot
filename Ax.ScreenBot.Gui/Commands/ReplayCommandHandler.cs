﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using WindowsInput.Events;
using WindowsInput.Events.Sources;
using Action = Ax.ScreenBot.Model.Actions.Action;

namespace Ax.ScreenBot.Gui.Commands
{
    internal sealed class ReplayCommandHandler
    {
        public async Task StartReplayAsync(
            ReplayEngine replayEngine,
            ObservableCollection<Action> storedActions,
            uint replayCount)
        {
            if (storedActions.Count == 0)
                return;

            using var keyboard = WindowsInput.Capture.Global.KeyboardAsync();
            keyboard.KeyEvent += (o, args) => StopIfPausePressed(args, replayEngine);
            await DoReplay(replayEngine, storedActions, replayCount);
            PlayNotificationSound();
        }

        private static void StopIfPausePressed(EventSourceEventArgs<KeyboardEvent> args, ReplayEngine replayEngine)
        {
            if (args.Data?.KeyDown?.Key == KeyCode.Pause)
                replayEngine.Stop();
        }

        private static async Task DoReplay(ReplayEngine replayEngine, ObservableCollection<Action> storedActions,
            uint replayCount)
        {
            try
            {
                await replayEngine.StartAsync(storedActions, replayCount);
            }
            catch (TaskCanceledException)
            {
                /* If it is cancelled, it's okay. */
            }
        }

        private static void PlayNotificationSound()
        {
            System.Windows.Media.MediaPlayer mp = new System.Windows.Media.MediaPlayer();
            mp.Open(new Uri(@"C:\windows\media\notify.wav"));
            mp.Volume = 1;
            mp.Play();
        }
    }
}