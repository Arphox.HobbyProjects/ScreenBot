﻿namespace Ax.ScreenBot.Gui.Extensions
{
    internal static class IntegerPointExtensions
    {
        internal static System.Drawing.Point ToDrawingPoint(this (int x, int y) point)
        {
            return new System.Drawing.Point(point.x, point.y);
        }
    }
}